# <a name="header"></a>Uploadcare Widget for AngularJS

This guide will walk you through the steps to empower your AngularJS apps with [Uploadcare Widget][uc-features-widget].

FIXME add intro/reasoning behind Angular integration

The Uploadcare Widget that we are going to use, is an  AngularJS 2+ wrapper `ngx-uploadcare-widget` with a number of  Uploadcare-specific attributes and events to offer (we'll explore these in more detail in the sections dealing with [attributes](#other-attributes) and [events](#events)).

## <a name="prerequisites"></a>Prerequisites

This guide assumes your familiarity with AngularJS and preinstalled dependencies for running Angular 2+ apps.

## <a name="installation"></a>Installation

To get started with our simple demo using Angular Uploadcare Widget let's first install `ngx-uploadcare-widget`:

```shell
npm install ngx-uploadcare-widget
```

## <a name="importing-deps"></a>Importing dependencies

We'll add the following line to our imports in the main module so that we can use the Uploadcare Widget component:

```typescript
 import { UcWidgetModule } from 'ngx-uploadcare-widget';
```

Note that we'll also have to include `UcWidgetModule` in our ``@NgModule` import list:

```typescript
@NgModule({
  imports: [
    ...,
    UcWidgetModule,
  ],
  ...
})
...
```

## <a name="configuration"></a>Uploadcare Widget Configuration

The next important thing to note is that The Angular Uploadcare Widget comes in two flavors to suit your needs: first, `ngx-uploadcare-widget` with provided default markup and, optionally, a barebones `ngx-uploadcare-widget-custom`. For simplicity in our code samples we are going to use the default `ngx-uploadcare-widget`.

### <a name="public-key"></a>Set up an Uploadcare public API key

Before we can proceed with our demo we first need to set the `public-key` attribute of `ngx-uploadcare-widget` (or, alternatively, `ngx-uploadcare-widget-custom` if you choose so). For more details on Uploadcare API keys, please, refer to [Uploadcare documentation](uc-docs-keys).

You basically have two options: either obtain a public key through sign-up procedure with your Uploadcare user account, or rely on a ``'demopublickey'`` for testing purposes only (you may get further information on guarantees for a test account and data expiration policies [here][FIXME].

Provided, that the variable `publicKey` has been defined something like below:

```typescript
export class AppComponent {
  publicKey = 'YOUR_PUBLIC_KEY';
  ...
}
```

We could have the following configuration for `ngx-uploadcare-widget` element in our template:

```html
<ngx-uploadcare-widget
        images-only={{isImageOnly}}
        public-key={{publicKey}}
        ...>
</ngx-uploadcare-widget>
```

### <a name="other-attributes"></a>Configure other Uploadcare Widget attributes

Among other attributes to configure the Uploadcare Widget behavior you will find:

* [public-key][uc-docs-widget-options-public-key]
* [multiple][uc-docs-widget-options-multiple]
* [multiple-max][uc-docs-widget-options-multiple-max]
* [multiple-min][uc-docs-widget-options-multiple-min]
* [images-only][uc-docs-widget-options-images-only]
* [preview-step][uc-docs-widget-options-preview-step]
* [crop][uc-docs-widget-options-crop]
* [image-shrink][uc-docs-widget-options-image-shrink]
* [clearable][uc-docs-widget-options-clearable]
* [tabs][uc-docs-widget-options-tabs]
* [input-accept-types][uc-docs-widget-options-input-accept-types]
* [preferred-types][uc-docs-widget-options-preferred-types]
* [system-dialog][uc-docs-widget-options-system-dialog]
* [secure-signature][uc-docs-widget-options-secure-signature]
* [secure-expire][uc-docs-widget-options-secure-expire]
* [value][uc-docs-widget-predefined]
* [cdn-base][uc-docs-widget-options-cdn-base]
* [do-not-store][uc-docs-widget-options-do-not-store]
* [validators][uc-docs-widget-validators]

Please, see [widget docs][uc-docs-widget-config] for other details.

## <a name="events"></a>Uploadcare Widget Events

Now it's time to define event handlers for the following Uploadcare Widget events:

* [on-upload-complete][uc-docs-js-api-widget-on-upload-complete]
* [on-change][uc-docs-js-api-widget-on-change]
* on-progress

### <a name="add-handlers"></a>Add event handlers for Uploadcare Widget events

Let's first define `onUpload` and `onProgress` functions to add simple logging behavior on triggered `on-upload-complete` and `on-progress` events respectively:

```javascript
  onUpload(info) {
    console.log('fired Event "onUpload"');
    console.log(info);
  }

  onProgress(progress) {
    console.log('fired Event "onProgress with data:"');
    console.log(progress);
  }
```
We'll next add some logic for `on-change`, which allows us to capture a new file selection event.
Here we'll also log fired event to console and in case multiple files have been selected,
we'll resolve either a promise or an array of promises for each of the uploaded files into an uploaded file group info.

```javascript
  onChange(file) {
    if(!file) {
      return;
    }
    console.log('fired Event "onChange"');
    if(this.multipleFiles) {
      console.log(file);
      if(file.promise) {
        file.promise().then((groupInfo) => {
          console.log('resolved general promise from "onChange" with data:');
          console.log(groupInfo);
        });
      }
      if(file.files) {
        file.files().forEach((promise) => {
          promise.then((fileInfo) => {
            console.log('resolves file promise with file info:');
            console.log(fileInfo);
          });
        });
      } else {
        console.log(file);
      }
    }
  }

```

### <a name="bind-events"></a>Bind the Uploadcare Widget events

Let's bind the Uploadcare Widget events in the template definition:

```html
<ngx-uploadcare-widget
...
(on-upload-complete)="onUpload($event)"
(on-change)="onChange($event)"
(on-progress)="onProgress($event)">
</ngx-uploadcare-widget>
```

## <a name="methods"></a>Uploadcare Widget Methods

Here's a short overview of the methods provided by Angular Uploadcare Widget:

* `clearUploads()` - Removes all current uploads from the widget. You can use the method to reset a form even if a user has already uploaded some files.
* `reset(clearUploads = false)` - Resets the widget, You can also remove all the current uploads if `clearUploads` is set to `true`
* `openDialog()` - Opens Uploadcare widget dialog with current configuration.
* `reject()` - Closes the dialog opened with `openDialog()` method only and discards any file selection.

To illustrate one of the above, let's suppose we want to associate the 'open dialog' logic with a button.
We could add the following button element to handle the click action:

```html
<button class="btn btn-info" (click)="openDialog()">Open dialog</button>
```
FIXME illustrate other methods and give screenshots

<!-- Links -->
[uc-features-widget]: https://uploadcare.com/features/widget/?utm_source=github&utm_campaign=ngx-uploadcare-widget
[uc-docs-keys]:https://uploadcare.com/docs/keys/#keys
[uc-docs-widget-config]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget
[uc-docs-widget-predefined]: https://uploadcare.com/docs/uploads/widget/predefined/?utm_source=github&utm_campaign=ngx-uploadcare-widget
[uc-docs-widget-options-public-key]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-public-key
[uc-docs-widget-options-multiple]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-multiple
[uc-docs-widget-options-multiple-max]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-multiple-max
[uc-docs-widget-options-multiple-min]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-multiple-min
[uc-docs-widget-options-images-only]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-images-only
[uc-docs-widget-options-preview-step]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-preview-step
[uc-docs-widget-options-crop]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-crop
[uc-docs-widget-options-image-shrink]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-image-shrink
[uc-docs-widget-options-clearable]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-clearable
[uc-docs-widget-options-tabs]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-tabs
[uc-docs-widget-options-input-accept-types]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-input-accept-types
[uc-docs-widget-options-preferred-types]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-preferred-types
[uc-docs-widget-options-system-dialog]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-system-dialog
[uc-docs-widget-options-secure-signature]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-secure-signature
[uc-docs-widget-options-secure-expire]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-secure-expire
[uc-docs-widget-options-cdn-base]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-cdn-base
[uc-docs-widget-options-do-not-store]: https://uploadcare.com/docs/uploads/widget/config/?utm_source=github&utm_campaign=ngx-uploadcare-widget#option-do-not-store
[uc-docs-widget-validators]: https://uploadcare.com/docs/api_reference/javascript/file_validation/
[uc-docs-js-api-widget-on-upload-complete]: https://uploadcare.com/docs/api_reference/javascript/widget/?utm_source=github&utm_campaign=ngx-uploadcare-widget#widget-on-upload-complete
[uc-docs-js-api-widget-on-change]: https://uploadcare.com/docs/api_reference/javascript/widget/?utm_source=github&utm_campaign=ngx-uploadcare-widget#widget-on-change
